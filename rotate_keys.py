import sys
import time
import boto3
import argparse
import getpass

# Get user role to rotate
parser = argparse.ArgumentParser()
parser.add_argument("user", help="Enter a user role to rotate.")
args = parser.parse_args()

print("Connecting to IAM...")

# Set IAM client
client = boto3.client("iam")

# Get current key
paginator = client.get_paginator("list_access_keys")

try:
    for response in paginator.paginate(UserName=args.user):
        for i in response["AccessKeyMetadata"]:
            old_access_id = i["AccessKeyId"]
except Exception as e:
    print("Error: {}".format(e))
    sys.exit(1)

# Create new keys for user
response = client.create_access_key(UserName=args.user)

# Save new keys
new_access_id = response["AccessKey"]["AccessKeyId"]
new_secret_key = response["AccessKey"]["SecretAccessKey"]

# Resting a few seconds while keys are made active
time.sleep(20)

# Create a Secrets Manager object
secrets_manager_test = boto3.client(
    "secretsmanager",
    aws_access_key_id=new_access_id,
    aws_secret_access_key=new_secret_key,
)

# Test the credentials
# If they aren't good, dump them and exit
try:
    response = secrets_manager_test.list_secrets()
except Exception as e:
    print("Error: {}".format(e))
    print("Secrets testing could not be validated. Exiting.")
    response = client.delete_access_key(UserName=args.user, AccessKeyId=new_access_id)
    sys.exit(1)

# Make old key inactive
response = client.update_access_key(
    UserName=args.user, AccessKeyId=old_access_id, Status="Inactive"
)

# Delete old key
response = client.delete_access_key(UserName=args.user, AccessKeyId=old_access_id)

print("Updating AWS CLI configuration...")

# Get user
user = getpass.getuser()

# Write keys to the credentials file
with open("/home/{}/.aws/credentials".format(user), "w") as f:
    f.write(
        "[default]\naws_access_key_id={}\naws_secret_access_key={}".format(
            new_access_id, new_secret_key
        )
    )